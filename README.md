DTU WEC controller Simulink model

DTU Wind Energy
Thanasis Barlas

Basic summary of contents:
- DTU WEC wind turbine controller (with derating and yaw control features)
- simple 1-DOF drivetrain WT model with Cp/Ct tabulated input
- IEA 15MW RWT modelled 

Quick user guide:
- set input in 'WEC_input.m'
- open 'WEC.mdl' and run
- basic variables are plotted in scopes

References:
- Meng F, Lio WH, Barlas T. DTUWEC: an open-source DTU Wind Energy Controller with advanced industrial features. Journal of Physics: Conference Series. 2020;1618. 022009. https://doi.org/10.1088/1742-6596/1618/2/022009
- Hansen MH, Henriksen LC. Basic DTU Wind Energy controller. DTU Wind Energy, 2013. 43 p. (DTU Wind Energy E; No. 0028)
- https://gitlab.windenergy.dtu.dk/OpenLAC/BasicDTUController
