% Input for WDTU WEC controller Simulink model

%% cleaning up

close all
clear all
clc

%% simulation

deltat = 0.01;                         % [s] sample time
t_start = 0;                           % [s] start time
t_end = 600;                           % [s] end time
V_init = 9;                            % [m/s] wind speed
Vdir_init = 0;                         % [deg] wind direction
yaw_ref = 0;                           % [deg] ref yaw error setpoint
pwt_ref = 1;                           % [-] ref power setpoint

%% turbine parameters

wt_data_file = 'WT_data_IEA_15MW_RWT'; % [-] Cp,Ct,TSR,pitch data file
wt_data_dr_file = 'drdata.dat';        % [-] derating Cp,Ct,TSR,pitch data file
J= 0.313227E+09;                       % [kg*m^2] rotor-drivetrain inertia
r = 120;                               % [m] rotor radius
rho = 1.225;                           % [kg/m^2] air density
neff = 0.9655;                         % [-] drivetrain efficiency
Cp_max = 0.4892;                       % [-] max Cp (for available power)
omega_init = 0.5236;                   % [rad/s] initial rotor speed
pitch_init = 0;                        % [deg] initial pitch angle
WTdir_init = 0;                        % [deg] initial WT direction
region_init = 2;                       % [-] initial control region (2 = partial load, 3 = full load)

%% turbine controller

pitch_file = 'wpdata.100';
Prated = 15e6;                         % [W] rated generator power
omega_min = 0.5236;                    % [rad/s] minimum rotor speed
omega_rated = 0.7917;                  % [rad/s] rated rotor speed
f_omega = 1*2*pi;                      % [rad/s] rotor speed filter frequency
zeta_omega = 0.7;                      % [-] rotor speed filter damping ratio
pitch_min_in = 100;                    % [deg] minimum pitch angle (>90 input file wpdata.100 is used)
pitch_max = 90;                        % [deg] maximum pitch angle
Kopt = 0.313309E+08;                   % [N*m/((rad/s)^2)] torque-speed gain
Kp_torque = 0.137765E+09;              % [N*m/(rad/s)] proportional gain of torque controller
Ki_torque = 0.309143E+08;              % [N*m/rad] integral gain of torque controller
Kp_pitch = 0.150677E+01;               % [1/s] proportional gain of pitch controller
Ki_pitch = 0.286555E+00;               % [-] integral gain of pitch controller
KK1 = 11.32216;                        % [deg] coefficient of linear term in aerodynamic gain scheduling
KK2 = 421.51156;                       % [deg^2] coefficient of quadratic term in aerodynamic gain scheduling
tau_vf = 1.0472;                       % [rad/s] time constant of 1st order filter on wind speed used for minimum pitch
tau_pitch = 0.5236;                    % [rad/s] time constant of 1st order filter on pitch angle for gain scheduling
rel_sp_open_Qg = 0.95;                 % [-] percentage of the rated speed when the torque limits are fully opened
TorqueCtrlRatio = 1;                   % [-] generator control switch (0 = constant torque, 1 = constant power)
pitch_lim = 2;                         % [deg] lower angle above lowest minimum pitch angle for switch
deratingMode = 3;                      % [-] derating mode (1 = constant rotor speed, 2 = max rotor speed, 3 = min Ct)
f_gen = 20*2*pi;                       % [rad/s] rotor speed filter frequency
zeta_gen = 0.9;                        % [-] rotor speed filter damping ratio
f_pitch = 1*2*pi;                      % [rad/s] pitch actuator filter frequency
zeta_pitch = 0.7;                      % [-] pitch actuator filter damping ratio
f_yaw = 0.2*2*pi;                      % [rad/s] yaw actuator filter frequency
zeta_yaw = 0.7;                        % [-] yaw actuator filter damping ratio
yaw_ma_start = 10.0;                   % [s] yaw start moving average window
yaw_ma_end = 1.0;                      % [s] yaw end moving average window
yaw_thres_start = 1.0;                 % [deg] yaw error threshold for starting yaw action
yaw_thres_end = 0.5;                   % [deg] yaw error threshold for stopping yaw action
yaw_rate = 0.4;                        % [deg/s] yaw rate limit
dP_wt_ref_max = 0.2;                   % [p.u./sec] WT power reference maximum ramp rate
dP_wt_ref_min = -0.2;                  % [p.u./sec] WT power reference minimum ramp rate

%% processing

% load WT data
load(wt_data_file);

% load min pitch data
if pitch_min_in>90
    formatSpec = '%f';
    wpdata = dlmread(pitch_file, formatSpec, 1, 0);
end

% load derating data
wt_data_dr= dlmread(wt_data_dr_file, formatSpec, 1, 0);
